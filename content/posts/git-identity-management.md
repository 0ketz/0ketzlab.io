+++
title = "Managing multiple git identities"
date = 2020-06-09T15:14:54+11:00
author = "ketz"
toc = true
background = "linear-gradient(rgba(0,0,0,.85), rgba(0,0,0,.85)) fixed, url('https://media.giphy.com/media/l0IykOsxLECVejOzm/giphy.gif') center/cover no-repeat fixed, black center center fixed"
description = "A brief discussion about an easy way to manage multiple identities while using git for source control management."
tags = ["opsec", "identity", "git", "scm", "development"]
+++

# Split concerns, split personalities

It's not uncommon these days for developers to have multiple identities with
which to commit their code. Perhaps a set of credentials for their employer,
another for their personal projects, and yet another if they are a student and
need to connect to university-supplied servers.

Unfortunately git takes a fairly unstrict / unstructured approach to identity
as it can be setup on a system with a single string. This means that services
such as Bitbucket, Gitlab and Github may interpret even the slightest
imprecision in configuration to represent a completely new identity. In
addition to this, it can become very easy to add commits to a work repository
using your personal email address if you aren't careful.

Depending on your use case it might be important to ensure that your identities
are kept separate from each other, particularly if you have email addresses
that you don't want to have exposed in public git repo commit histories. It's
for this reason that I use the setup that I'm about to describe. It's pretty
easy to setup and it works really well, let's get started.

{{<spacer>}}

# How does it work?

Without going too much into the technicalities of git, there are a few things
you should know to help you understand the way this setup works.

**Git has three levels of configuration:**

- System
  - This is usually defined somewhere like `/etc/gitconfig` _(or similar
      depending on your system)_.
  - The settings that are made here apply to all users of the system.
      Generally it's not necessary to change this too much on a personal
      machine.
- Global
  - This is usually defined by a user's home directory: `~/.gitconfig`, _or as
      I discovered whilst writing this_, alternatively: `~/.config/git/config`.
  - The settings defined in this file are used for the particular user that
      this `$HOME` directory belongs to. Normally you would set your identity
      configuration here.
- Local
  - This is a git config that lives _inside_ each git repository (in
      `.git/config`).
  - As you may expect, this is configuration that applies only to the git
      repository in question.

However, these are only the config files that _git will look for **by default**_. It is
actually possible for us to use the above configuration files to specify locations
from which to source additional configuration _conditionally_ using
the `includeIf` directive, and that's what we're going to do here.

## The setup

We need to setup a directory structure similar to this:

```
~
└── Source
  ├── home
  ├── uni
  └── work
```

The names don't matter too much, however I'm pretty sure it _is_ important that
your source code lives in a directory __underneath__ your home directory
(as in: `~/$something`).

## Adding some configuration magic

Next we want to open up your existing __global__ configuration file and add a
few items to the bottom of it[^1].

[^1]: It should definitely be at the end of this file, otherwise anything
  specified underneath could "re-override" the configurations you've set within
  your directories :scream:

```gitconfig
...

[includeIf "gitdir:~/Source/home/"]
    path = ~/Source/home/.gitconfig

[includeIf "gitdir:~/Source/uni/"]
    path = ~/Source/uni/.gitconfig

[includeIf "gitdir:~/Source/work/"]
    path = ~/Source/work/.gitconfig
```

Hopefully you can see where we're going with this. We've added directives in
the global configuration file so that when git is running underneath the specified
directory[^2] _(the "gitdir:~/..." one)_, it makes sure to also
look for a configuration file that is available at the relevant `path` option.

[^2]: This is also recursive, meaning that it will apply to anything
  underneath
  the _"gitdir:..."_ directory, and also any subdirectories underneath those too.

So for example if I'm working in `~/Source/uni/my-great-project/`, git will
source configuration first from the global config file, and then by extension,
it will merge the contents of the `~/Source/uni/.gitconfig` file on top of that
configuration.

These directory-based git configuration files work exactly the same as the
standard global configuration file, with the same options and everything,
except they will only apply to the subfolders underneath them.

{{<spacer>}}

## An example

Here are some example config files to show the idea.


<label>`~/.config/git/config`</label>
```gitconfig {linenos=true,linenostart=45}
...

[includeIf "gitdir:~/Source/home/"]
    path = ~/Source/home/.gitconfig

[includeIf "gitdir:~/Source/work/"]
    path = ~/Source/work/.gitconfig
```

<label>`~/Source/home/.gitconfig`</label>
```gitconfig {linenos=true}
[user]
  name = "Super Ketz"
  email = "supercool.email@ketz.internet"
```

<label>`~/Source/work/.gitconfig`</label>
```gitconfig {linenos=true}
[user]
  name = "Business Cat"
  email = "formal.cat@importantbusiness.com"
  signingkey = 0A46826A
```
<small class="caption o-50">Interested in <code>signingkey</code>?[^3]</small>

[^3]: If you are interested by what `signingkey` is, you can find out more
  about it [here](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work).
  Essentially it is a way to configure GPG keys to cryptographically sign your
  commits, and therefore proving that it was almost certainly you that made a
  commit.

...and if we are playing around in a terminal window a bit, it might look like
this _(I've added spaces to help the visuals)_:

```bash
~ > pwd
/home/user

~ > git config --get user.name
Casual Cat

~ > cd Source/home

~/Source/home > git config --get user.name
Super Ketz

~/Source/home > cd ../work

~/Source/work > git config --get user.name
Business Cat

~/Source/work > █
```

{{<spacer>}}

# Conclusion

I have successfully been using this approach for about two years now and it
works very well for me. It might fall over if you are stuck working in a
particular directory, for example developing something in
go and are unable for some reason to use [go modules](https://blog.golang.org/using-go-modules).

Personally I am yet to encounter a situation like this and so this setup will
remain in use for the forseeable future. Hopefully these concepts provide you a
way to more easily manage git identities and stop different worlds blurring
into each other.
