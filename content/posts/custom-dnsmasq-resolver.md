+++
title = "Setting up a custom dnsmasq resolver"
date = 2020-01-15T21:39:54+11:00
author = "ketz"
toc = true
background = "linear-gradient(rgba(0,0,0,.85), rgba(0,0,0,.85)) fixed, url('https://media.giphy.com/media/UwLaDyq7uwcuc/giphy.gif') center/cover no-repeat fixed, black center center fixed"
description = "A quick guide on how to use dnsmasq on a linux workstation to provide personal DNS resolution for arbitrary hosts."
tags = ["dns", "dnsmasq", "network-manager", "kali", "linux", "networking"]
+++

> ***DISCLAIMER:*** I have **NOT** looked into the full extent of implications
> that this configuration results in. There could be unintended side-effects
> that may be dangerous. I recommend that you evaluate the effects of the
> changes _before_ carrying them out.
>
> Also, I used a clean instance of **[Kali 2019.4](https://cdimage.kali.org/kali-2019.4/kali-linux-2019.4-amd64.iso)**.


# Intro

Setting up a custom resolver using `dnsmasq` is pretty quick and easy to do.
With an additional amount of special scripting sauce, it can be used to elevate
your workflow when dealing with IP addresses that may be used for multiple
vhosts (like different subdomains `www.miau.io`, `git.miau.io`, etc).

## Rationale

I came across this because I was looking for a solution that would:

- Help me easily associate a domain and all its subdomains with a single IP address.
- Not involve messing around with large convoluted text files (I use `/etc/hosts` to block ads by DNS).
- Be easily scriptable.
- Not affect any other network devices (had to apply only locally).

My use case was for playing [hackthebox](https://hackthebox.eu/), and attacking
machines that served multiple services on different subdomains from the same IP
address, however I'm sure that this solution could be used for more varied
purposes as it essentially gives you your own private DNS server.

In short, this is a slightly better way to deal with resolving of custom hosts
that doesn't involve messing around with `/etc/hosts`. The other side to this
argument is that it may require you to run additional processing to run
successfully so if your system requirements are lacking, then it may be better
to stick with the old methods.

# Steps

## 1. Update `network-manager` configuration

Using your favourite editor, update the
`/etc/NetworkManager/NetworkManager.conf` file. You will want to add an entry
under the `[main]` section which instructs NetworkManager to use dnsmasq to
cache DNS results _(see line 3 below)_.

```cfg {linenos=true,hl_lines=[3]}
[main]
plugins=ifupdown,keyfile
dns=dnsmasq

[ifupdown]
managed=false
```

Restart the `network-manager.service` using the command below.

```bash
>> sudo systemctl restart network-manager.service
```

NetworkManager will now restart and additionally spawn a process for
dnsmasq[^first]. We now have our DNS caching server running, so the next step
is to add some custom records to it so that we can resolve the names that we
choose.

[^first]: This appears to only work as long as the machine has an external
  network connection.

    There may be another way to get around this, but until you have an external
    connection, running dnsmasq is probably not particularly useful.


## 2. Add some DNS configuration

In the `/etc/NetworkManager/dnsmasq.d/` directory, add a new `*.conf` file
which will be where dnsmasq will look for your custom records.

Subsequently, add a line to this file to add a record. In the example below, a
wildcard entry for anything belonging to `*.wildcard.box` will resolve to the
`10.10.10.1` IP address. This is because the name is prefixed by a `.`
character.

```cfg {}
address=/.wildcard.box/10.10.10.1
```

The lines in this file can be used for much more than just adding address
options, but that is as far as is worth mentioning for the purposes of this
article[^second].

[^second]: You can see more about dnsmasq configuration options [here](http://www.thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html#lbAF).

## 3. Restart the dnsmasq process

I've found that the easiest way to do this is to kill the dnsmasq process, and
let NetworkManager restart it automatically. This causes the program to reload
the settings and also clear out the cache. This can be achieved with the
following command.

```bash
>> sudo killall dnsmasq
```

## 4. Test that the configuration is working

Use the `dig` command to check that the local dnsmasq server is catching the
provided address pairings from the configuration file.

```none
>> dig wildcard.box

; <<>> DiG 9.11.5-P4-5.1+b1-Debian <<>> wildcard.box
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 4518
;; flags: qr aa rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;wildcard.box               IN      A

;; ANSWER SECTION:
wildcard.box        0       IN      A       10.10.10.1

;; Query time: 0 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Thu Jan 02 18:37:24 AEDT 1994
;; MSG SIZE  rcvd: 61
```

# Caveats

Things _(I think)_ I have found out whilst testing this configuration:

- `NetworkManager` won't start a `dnsmasq` process if you do not have a live network connection other than localhost.
- Any entries added to the configuration will _override_ any real world DNS records (this might go without saying for some, but I think it bears mentioning).
- It is another process running on your machine, therefore it uses more memory. Not a big deal if you have heaps to play with, but `14MB` can be a pretty big deal on a Raspberry Pi.
- By default on Kali, the dnsmasq instance built into NetworkManager is set to run on localhost only, but you should probably double check that this is the case or you may be leaking your "internal" server onto other networks.

# Conclusion

This is a different, but not necessarily better way of dealing with localized
DNS resolution, in comparison with using `/etc/hosts`. I would consider it
easier to manage and also with the additional benefit of being extendable with
the other powerful features that dnsmasq has to offer[^second].

This approach is easy to activate, and generally requires no extra dependencies
as long as your distribution is using NetworkManager.

As soon as your local dnsmasq instance can't find a DNS query it will defer to
`/etc/hosts` and then the DNS servers defined in your network connections,
therefore aside from the modifications that **you** make, it should be entirely
unnoticable.

{{< spacer >}}

# _bonus round: bash script_ (WIP)

With some bash scripting it makes adding custom hosts to dnsmasq much quicker.
You could do something similar to this if you wanted to:

```bash {linenos=true}
#!/bin/bash

# dnsmasq-update.sh
# by ketz

# Usage: ./dnsmasq-update.sh <domain> <host>

domain=$1
host=$2
config_path="/etc/NetworkManager/dnsmasq.d/"
dom_array=(`echo $domain | tr '.' '\n'`)
tld=${dom_array[${#dom_array[@]}-1]}

if [ $# -eq 2 ]; then
    mkdir -p $config_path 2> /dev/null;
    touch ${config_path}${tld}".conf" 2> /dev/null;
    echo "address=/.$domain/$host" >> ${config_path}${tld}".conf";
    sudo killall dnsmasq;
else
    echo "ERR: Incorrect arguments.";
    exit 1;
fi
```

This script stores the configuration for each top level domain in their own
configuration file under `/etc/NetworkManager/dnsmasq.d/`, this makes it easier
to read and manage later on. You could use this to create a custom TLD for a
particular engagement and then simply delete the file once it is no longer
relevant.
