---
title: "about"
url: "/about"
background: "linear-gradient(rgba(0,0,0,.75), rgba(0,0,0,.8)) fixed, url('https://media.giphy.com/media/Bi6FcO7UoutWM/giphy.gif') center/cover no-repeat fixed, black center center fixed"
date: 1970-01-01T00:00:00+00:00
---

I go by the name ***ketz***. I like infosec, cats and the color blue.
I play __CTFs__ and __Hack The Box__ in my spare time and occasionally help out with the
Melbourne HTB meetup.

You can get in contact with me by <a href="https://twitter.com/messages/compose?recipient_id=1026437097534631936" title="Opens to a new compose window on twitter to send me a message" target="_blank" style="color: #1da1f2;">sending a DM on twitter</a>.

<img
  src="/ketz.svg"
  class="bg-white mt4 w-45-m w-25-l db w-40-ns w-100 tc br3 pa1-ns pa4 border-box"
  alt="Cartoon cat drawing">
