+++
title = "DownUnder<wbr>CTF"
date = 2020-09-20T18:23:21+10:00
author = "ketz"
background = "linear-gradient(rgba(0,0,0,.85), rgba(0,0,0,.9)) fixed, url('https://media.giphy.com/media/3o7qE5866bLg4VKabe/giphy.gif') center/cover no-repeat fixed, black center center fixed"
slug = "downunder-ctf"
toc = true
tags = ["jeopardy", "ctf", "osint", "misc", "gpg", "python", "pickle"]
description = "A 48-hour jeopardy ctf, a replacement for the CySCA that never was."
+++

# Introduction

Once upon a time there was an annual event put on by the Australian Government
called **<abbr title="Cyber Security Challenge Australia">CySCA</abbr>**. I was
lucky enough to take part in this event in 2018 (the last year it has been run
to date) and it was an excellent competition amongst the TAFEs and universities
across Australia. The challenges were difficult and were a great learning
opportunity.

Sadly it's currently unknown whether or not CySCA will ever run again, but
thankfully a coalition of university infosec clubs have banded together to put
on an equivalent event.

## Shoutouts

Naturally none of this is possible without people, and they deserve credit.

### Organisers

A big thanks to the following groups for helping to put this on:

<ul>
    <li>Curtin Cyber Security Society (C-SEC)</li>
    <li><a target="_blank" rel="noopener" href="https://deakininfosec.com.au">Deakin University Information Security Club (DISC)</a></li>
    <li><a target="_blank" rel="noopener" href="https://macs.codes">Macquarie University Cyber Security Society (MQ CyberSec)</a></li>
    <li><a target="_blank" rel="noopener" href="https://monsec.io">Monash University Cyber Security Club (MonSec)</a></li>
    <li><a target="_blank" rel="noopener" href="https://qutwhitehats.club">QUT Whitehats Society</a></li>
    <li><a target="_blank" rel="noopener" href="https://risc.melbourne">Royal Melbourne Institute of Technology Information Security Club (RISC)</a></li>
    <li><a target="_blank" rel="noopener" href="https://scsc.io">Swinburne Cyber Security Club (SCSC)</a></li>
    <li><a target="_blank" rel="noopener" href="https://syncs.org.au">Sydney Computing Society (SYNCS)</a></li>
    <li>TahSec</li>
    <li><a target="_blank" rel="noopener" href="https://www.umisc.info">University of Melbourne Information Security Club (MISC)</a></li>
    <li><a target="_blank" rel="noopener" href="https://unswsecurity.com">UNSW Security Society (SecSoc)</a></li>
    <li><a target="_blank" rel="noopener" href="https://cybersquad.uqcloud.net">UQ Cyber Squad</a></li>
    <li><a target="_blank" rel="noopener" href="https://utscyber.org">UTS Cyber Security Society (CSEC)</a></li>
    <li>UWA Ethical Hacking</li>
</ul>

It's great to see a grassroots event establish itself in the midst of
everything going on this year. Despite the adversity these uni clubs have
made sure they have something that they can look forward to.

### Teammates

- **team**: `I LIKE WHAT YOU GOT. GOOD JOB!`
    - _PlanetsOnly_
    - _Markers_

{{<spacer "tl">}}

# Categories

| `category` | `count` | `description`    |
|:-----------|---------|:-----------------|
| OSINT      |  9      | Supreme google-fu skills required. |
| forensics  |  6      | Plugging around inside data files trying to find stuff. |
| web        |  7      | Web applications and techniques, browser business. |
| crypto     |  9      | Number crunching and ciphers etc. |
| pwn        |  7      | Buffer overflows and software short-circuting. |
| reversing  |  4      | Decompiling, deconstructing and debugging. |
| misc       |  13     | Everything that isn't anything else. |

{{<spacer "tl">}}

# Challenges

## Twitter

{{<ctf/container points="10" category="misc" flag="DUCTF{https://www.youtube.com/watch?v=XfR9iY5y94s}">}}

<span class="label">Description</span>

> Check out our Twitter! Find the post with the flag! You can give us a follow if you like <3

<span class="label">Method</span>

Go to twitter, find tweet.

{{<tweet 1287018872457977856>}}

It's base64, so we go to [CyberChef](https://gchq.github.io/CyberChef/) for an
[easy conversion](https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)&input=UkZWRFZFWjdhSFIwY0hNNkx5OTNkM2N1ZVc5MWRIVmlaUzVqYjIwdmQyRjBZMmcvZGoxWVpsSTVhVmsxZVRrMGMzMD0).

Heh, that's a good flag.

{{< youtube XfR9iY5y94s>}}

{{</ctf/container>}}

{{<spacer "left">}}


## On the spectrum

{{<ctf/container points="100" category="forensics" flag="DUCTF{m4by3_n0t_s0_h1dd3n}">}}

<span class="label">Description</span>

> My friend has been sending me lots of WAV files, I think he is trying to communicate with me, what is the message he sent?

<span class="label">Method</span>

_"Spectrum"_ is referenced by the title of this task, so my assumption is that
it is a spectrogram-related task. Pretty low on the points scale too compared
to other challenges, so I fire up [Sonic Visualiser](https://sonicvisualiser.org).

Definitely something going on down the low end of the frequency range, and
after messing about a little with the spectrogram settings, I got a readable
flag.

![The flag as viewed in Sonic Visualiser](./images/on_the_spectrum.png)

{{</ctf/container>}}

{{<spacer "left">}}


## Bad man

{{<ctf/container points="200" category="OSINT" flag="DUCTF{w4y_b4ck_1n_t1m3_w3_g0}">}}

<span class="label">Description</span>

> We have recently received reports about a hacker who goes by the alias `und3rm4t3r`. He has been threatening innocent people for money and must be stopped. Help us find him and get us the flag.

<span class="label">Method</span>

We're talking about aliases here, so the first thing that's worth a shot is
checking out what services have the alias as a username in them. There's a
simple tool online called [Instant Username
Search](https://instantusername.com/#/) which checks for valid usernames
on different platforms _(although the accuracy is questionable at best, it's
usually good enough)_. I found a bunch of accounts, but the meatiest was
twitter.

Scanning over the tweets there is one that references accidentally posting
personal information and being glad that there was a delete button. So this is
a timeshifting challenge.

We go to the same page on the [Wayback Machine](https://web.archive.org/web/*/https://twitter.com/und3rm4t3r/)
and it looks like we luckily found the tweet that was deleted.

{{</ctf/container>}}

{{<spacer "left">}}


## 16 Home Runs

{{<ctf/container points="100" category="misc" flag="DUCTF{16_h0m3_run5_m34n5_runn1n6_p457_64_b4535}">}}

<span class="label">Description</span>

> How does this string relate to baseball in anyway? What even is baseball? And how does this relate to Cyber Security? ¯\(ツ)/¯
>
> ```
> RFVDVEZ7MTZfaDBtM19ydW41X20zNG41X3J1bm4xbjZfcDQ1N182NF9iNDUzNX0=
> ```

<span class="label">Method</span>

Moar base64, what a wonderful time!

Use [CyberChef](https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)&input=UkZWRFZFWjdNVFpmYURCdE0xOXlkVzQxWDIwek5HNDFYM0oxYm00eGJqWmZjRFExTjE4Mk5GOWlORFV6TlgwPQ)
to decode it and we've already hit the flag. Nice!

{{</ctf/container>}}

{{<spacer "left">}}


## In a pickle

{{<ctf/container points="200" category="misc" flag="DUCTF{p1ckl3_y0uR_m3554g3}">}}

<span class="label">Description</span>

> We managed to intercept communication between `und3rm4t3r` and his hacker friends. However it is obfuscated using something. We just can't figure out what it is. Maybe you can help us find the flag?
>
> ### Files
> - `data`

<span class="label">Method</span>

I have heard of Python pickling before, it's a Python way of object
serialization / deserialization. I've never done it before so I did a quick
google and it's a fairly straight forward process (at least in this case).

I started off by writing this script:

```python
import pickle

data = open("data", "rb")
out = pickle.load(data)

print(out)
```

Which gives us:

```python
{1: 'D', 2: 'UCTF', 3: '{', 4: 112, 5: 49, 6: 99, 7: 107, 8: 108, 9: 51, 10: 95, 11: 121, 12: 48, 13: 117, 14: 82, 15: 95, 16: 109, 17: 51, 18: 53, 19: 53, 20: 52, 21: 103, 22: 51, 23: '}', 24: "I know that the intelligence agency's are onto me so now i'm using ways to evade them: I am just glad that you know how to use pickle. Anyway the flag is "}
```

Reading between the lines we're about halfway there. We still need to convert
some of the items in the pickle because they are decimal representations of
ASCII characters and need to be converted to ASCII.

So let's change the approach a little bit. We can easily read the _"plaintext"_
of this elaborate encryption strategy, so we'll just ignore that for now. What
we really want to get is the actual contents of the flag. We need to convert
the decimal values to characters, and we can do this using Python's builtin
function: `chr()`.

```python
import pickle

data = open("data", "rb")
out = pickle.load(data)

decimals = range(4, 23)

for i in decimals:
    print(chr(out[i]), end="")
```

Now we get

```python
p1ckl3_y0uR_m3554g3
```

This looks like the meat that we're looking for in our flaggy sandwich, and it
turns out it is. We just need to wrap it in two slices of `DUCTF{` and `}`, and
we're golden, nice!

{{</ctf/container>}}

{{<spacer "left">}}


## Pretty Good Pitfall

{{<ctf/container points="200" category="misc" flag="DUCTF{S1GN1NG_A1NT_3NCRYPT10N}">}}

<span class="label">Description</span>

> PGP/GPG/GnuPG/OpenPGP is great! I reckon you can't find the message, because it looks scrambled!
>
> ### Files
> - `flag.txt.gpg`

<span class="label">Method</span>

This challenge is actually a good cautionary tale for folk who are new to GPG.
It makes a point of the fact that merely using GPG on a file and making the
insides of it scrambled does not make it _encrypted_.

Why is this? Well, it's possible to use GPG keys to merely _sign_ something,
meaning that it can easily be returned back to its plaintext state. Getting the
`flag.txt` out of this `flag.txt.gpg` is as simple as running:

```bash
$ gpg flag.txt.gpg
```

GPG then digests the _signed_ file back to its original format and we get our
flag as a new file: `flag.txt`. A good lesson of something that can easily go
wrong when using GPG if you haven't had much practice with it.

{{</ctf/container>}}

{{<spacer "left">}}

## Leggos

{{<ctf/container points="100" category="web" flag="DUCTF{n0_k37chup_ju57_54uc3_r4w_54uc3_9873984579843}">}}

<span class="label">Description</span>

> I <3 Pasta! I won't tell you what my special secret sauce is though!
>
> [https://chal.duc.tf:30101](https://chal.duc.tf:30101)

<span class="label">Method</span>

How do you view source when the web page won't let you? Hmmm, tricky indeed.
There are a few approaches you could take for this challenge. It was pretty
clear from all the hints about _sauce_ that we needed to view source.

The webpage that we were greated with prevented the common keystrokes to open
up devtools or to view source for the page. To get around this I added
`view-source:` in front of the URL and tried again.

I couldn't see the flag in the page contents, but a visit to the javascript
file that was blocking the keystrokes, was enough to get that delicious flag.


{{</ctf/container>}}

{{<spacer "left">}}


## Off the Rails 2: Electric Boogaloo: Far from Home

{{<ctf/container points="336" category="OSINT" flag="DUCTF{dunderland}">}}

<span class="label">Description</span>

> Okay so I'm starting to think I may have got on the wrong train again and now it's snowing so hard I have no clue where I am 😬. Thankfully, I managed to snap this pic before the storm started. We seem to have stopped at this station.. can you tell me it's name?
>
> _Please tell me the name of the station in lower case flag format, with any spaces replaced by underscores._
>
> ### Files
> - `no_pain_no_train.png`

<span class="label">Method</span>

So basically, we have a picture of a place in the world, and we need to figure
out where it is.

![The provided image for the challenge, no_pain_no_train.png](./images/no_pain_no_train.png)

This isn't really _heaps_ to go on, but luckily I have some experience in
trains.

The most iconic thing about this image is the Red building near the railway.
In Norway, many of the houses and railway buildings are distinctively coloured
red in this way. So I already knew it would be somewhere in __Norway__.

I spent a bit of time casually looking through every single train service in
Norway but this was not very effective, so I changed up the process a bit.

Knowing that it was in Norway, I determined via process of elimination
(essentially bruteforce) that the model of the train in the picture was a [NSB
Di 4](https://en.wikipedia.org/wiki/NSB_Di_4), owing to a combination of: the
positioning of the lights, the window shape, red colouring and distinctive
yellow snowplow.

The Wikipedia page for this particular class of locomotive says that it only
serves a single line, the _Nordland Line_. So we now have a pretty narrow field
left to search in, getting closer.

Another great thing that Norway does, is the so-called _Slow TV_ programs that
they create, very often being journeys via some form of transportation. These
programs were originally started out on the trains, and thankfully this
practice has spread far and wide across Norway, so we could find a video of the
Nordland line and review each station.

{{<youtube "3rDjPLvOShM?t=26380" >}}

I found the above video, and someone had kindly created a list of timestamps
for each station which sped up my search immensely, I clicked through each one
until I arrived at __Dunderland__. In fact, if you got to exactly
[7:19:40](https://youtu.be/3rDjPLvOShM?t=26380 "Timestamp 7:19:40 in the Nordland Slow TV video on YouTube") in that video, you get almost
exactly the same image as the one from the file.

We found the station! Awesome!

{{</ctf/container>}}

{{<spacer "left">}}

## Outback Stakeout

{{<ctf/container points="482" category="OSINT" flag="DUCTF{pine_gap_38}">}}

<span class="label">Description</span>

> My favourite place to grab a snack. Where is this and, how many dishes are there?
>
> _Please let me know in flag format with the location in lowercase and underscores instead of spaces, followed by the number of dishes:_ `DUCTF{location_dishcount}`
>
> ### Files
> - `dont_dish_out_what_you_cant_take.jpg`

<span class="label">Method</span>

We're given the following image.

![dont_dish_out_what_you_cant_take.jpg](./images/dont_dish_out_what_you_cant_take.jpg "The supplied image for the challenge. An oblique-view aerial photo of an outback setting with two parallel lines of hills and a small settlement between the two lines of hills")

Straight away I knew exactly what this was. But for the sake of a writeup,
let's think a bit about the context of this CTF event.

This is DownUnderCTF, celebrating everything about Australia, whilst also being
completely about infosec. This is an image of __Pine Gap__, a US surveillance
station that's situated in the Northern Territory, not far from Alice Springs.

{{</ctf/container>}}

{{<spacer "left">}}


# Summary

It's been a massive week for me, I started working in a new place and had to
get up to speed with heaps of new things for that so I wasn't feeling super
competitive for this event when Friday came around. Having said that, I think I
put a pretty decent effort into it without burning too many precious weekend
hours in front of a laptop.

This event had a great mix of challenges both in terms of category and
difficulty, and I think it's done a great job of replacing the idea of CySCA.
I think it actually became a bigger event than CySCA with a higher difficulty
curve towards the top end.

The results are up on [CTFtime](https://ctftime.org/event/1084), we ended up in
`171st` place with `1848 points`, which is a pretty pleasing result from an
event that I personally didn't put _too much_ effort into. I didn't really
learn much in the way of new stuff this time around because I stuck to the
things I was familiar with. Perhaps next time I'll look into weird and wacky
business and actually try to learn a new skill or two.

Thanks a lot to the organiser's for a great job, it'd be really cool to see
this become an annual event, but that's still something to be seen in the
future.

That's all for now, looking forwards to the next one :smiley:

## Graphs

<div class="tc db br4 pt4 pb2 bg-white flex items-center">
    <img class="dib br2 flex-auto w-40" src="./images/category-breakdown.png">
    <img class="dib br2 flex-auto w-40" src="./images/solve-percentages.png">
    <img class="dib br2 flex-auto w-90" src="./images/score-over-time.png">
</div>
