+++
title = "CYBAR OSINT CTF"
date = 2020-06-07T18:23:21+10:00
author = "ketz"
background = "linear-gradient(rgba(0,0,0,.85), rgba(0,0,0,.9)) fixed, url('https://media.giphy.com/media/3o6MbmvhgNSyHIu7oA/giphy.gif') center/cover no-repeat fixed, black center center fixed"
slug = "cybar-osint-ctf"
aliases = [
  "/writeups/cybar-osint-ctf/"
]
toc = true
tags = ["OSINT", "ctf", "geolocation", "google-fu", "google-dorks"]
description = "A 24-hour OSINT-based ctf, lots of really interesting and fun challenges, almost no terminal windows 😱"
+++

# About

This challenge was a 24-hour **[OSINT](https://en.wikipedia.org/wiki/Open-source_intelligence)**-based ctf. I found it was a lot more about
using your brain than using your computer, which was a really cool change of
pace. Some of the challenges were
<span class="tracked i b">absolutely outrageous</span> but that
just made the whole thing even better.

This is the [roundup](https://github.com/cybar-party/cybar-osint-ctf-2020/)
and these are the
[results](https://github.com/cybar-party/cybar-osint-ctf-2020/blob/master/Results.md).

## Thank you!

A huge shoutout to the following awesome people who made it all happen:

**Organisers / Mods**
- [@securitymeta_](https://twitter.com/securitymeta_)
- [@bl3ep](https://twitter.com/bl3ep)
- [@hotpheex](https://twitter.com/hotpheex)
- **OpSys**
- [...and many more](https://github.com/cybar-party/cybar-osint-ctf-2020/#credits)

_You are all wonderful people and put on probably the best CTF I have ever done._

**Teammates**
- **team**: `#!/bin/false`
  - [@Naraka](https://twitter.com/Naraka_ITSec)
  - **_"JakePaul"_**

_You two were both integral parts of the team and we wouldn't have ended up
where we did without you._

You're the best :heart:



{{<spacer "left">}}



# Categories

The challenges were broken down into three categories _(two if you don't count
the tutorial)_.[^first]

[^first]: The full list _(along with official writeups)_ is:
[here](https://github.com/cybar-party/cybar-osint-ctf-2020/blob/master/Challenges/README.md).

## Tutorial
  - `1 challenge`
  - a search engine warmup.
## General
  - `7 challenges`
  - Wider context, use public data sources to find answers.
## Social
  - `15 challenges`
  - Based on social media, traverse social networks to find more clues.



{{<spacer "left">}}



# Challenges

> _Just a quick note, some of the links in these writeups may break
> eventually. Unfortunately, the web is one big decaying monster_ :shrug:



## Tutorial Island

{{%ctf/container points="50" category="Tutorial" flag="CYBAR{YTCracker}" %}}

<span class="label">Description</span>

> Welcome to the CYBAR Open-Source Intelligence CTF. Most challenges can be solved with a browser and some know-how - online tools might help too.
>
> When you find something, whether it be a picture, text, code or whatnot, you can submit it into the CTF server by putting the 'flag format' around it. For example, if the flag asked you to find the name of a yellow fruit, and yo confirmed it was a banana, you'd submit "CYBAR{banana}". Don't worry too much about caps, it's not case-sensitive.
>
> To kick it off, let's try your first flag. You get into work, boot up your system and hit Spotify up. You can't remember the name of the artist or song you were listening to the other day but it put you in the ZONE. Scratching your head, you remember just a line of lyrics...
>
> "In the name of the Spam God, that's what's up"
>
> Huh. Let's do a quick search and see if we can find the artist, and submit as a flag. When done, you can kick off their playlist to pair with the CTF.

<span class="label">Method</span>

This was just a quick warmup task, so nothing super difficult here. Mods
were even kind enough to supply the correct
[google dork](https://en.wikipedia.org/wiki/Google_hacking) in the description.

Put {{<search "\"In the name of the Spam God, that's what's up\"">}}
into your favourite search engine and you're off to the races!

{{% /ctf/container %}}

{{<spacer "left">}}



## You've heard of elf on the shelf, but what about the proliferation of COVID-19?

{{%ctf/container points="50" category="Social" flag="CYBAR{HevisMarc}" %}}

<span class="label">Description</span>

>The Roombas are trying to gain the upper hand over the human population. We believe they're going to target pivotal industries such as real estate, critical infrastructure, information security and healthcare. We don't know who yet, but we know it's a group of close friends and all are infected with COVID-19.
>
>We need to enact Contact Tracing - finding every detail about their lives in order to predict and contain their movements. No one has heard from them since March. We must build up details about them for the agents to then take over. That's where you come in.
>
>Our first piece of intelligence is a gentleman by the name of Marc Hevis - a co-owner of Hevis Properties Pty Ltd. We have agents ready on the ground, and others covering all his other social media
> - your task is to find his Twitter account.

<span class="label">Method</span>

Essentially we have to find the existence of "Marc Hevis" on twitter. There are
a few different ways to do this, but I chose to search with: {{<search "site:twitter.com marc hevis">}}.

We can click on the first result which is for **[@HevisMarc](https://twitter.com/HevisMarc)**. We cross-reference
some information on his bio to confirm that this is the correct
person (he's the CTO of Hevis Properties according to the bio).

Another good clue in this instance is that the account has few tweets and was
recently created which suggests that it was purposely made for the CTF.

{{% /ctf/container %}}

{{<spacer "left">}}



## Contact Tracing - Part VII

{{%ctf/container points="50" category="Social" flag="CYBAR{Colombo}" %}}

<span class="label">Description</span>

> Pong may have travelled international recently, and we need you to find out which city he was in.

<span class="label">Method</span>

Okay, so to do this challenge we were kind of doing the whole _Contact
Tracing_ asynchronously as we chatted about it on Discord. So it was a little
out of order, but that didn't affect the task too much.

After scanning over all of the mutual followers of `@HevisMarc`, we knew that
Pong was a mutual connection and after searching on twitter with:
{{< search "!tw from:HevisMarc">}}
we can see that Marc has replied to one of Pong's tweets.

<div class="mv4 db mw-100">{{< tweet 1235036029033140224 >}}</div>
<div class="cb"></div>

This tweet seemed to be the most likely to suggest that Pong had been overseas.
By way of a fluke I recognised the insignia on the building as being similar to
the Sri Lankan Flag :flag-lk:.

As the building was quite large and _"imperial-looking"_. I searched on **duckduckgo** for
{{<search "sri lankan government building">}}, and after scrolling through the images I
found a building that had the same two distinctive inverted glass entry covers.

Interestingly, the correlating image was on an
[Austrian website](https://austria-forum.org/af/Geography/Asia/Sri_Lanka/Pictures/Colombo/Government_Building)
(who would have guessed?). The caption of the Austrian site said that it was taken in **Colombo**.

{{% /ctf/container %}}

{{<spacer "left">}}



## Contact Tracing - Part III

{{%ctf/container points="125" category="Social" flag="CYBAR{Wilson Botanic Park}" %}}

<span class="label">Description</span>

>We need more locations Alycee may have or will visit in the future.<br/>
>What is the first name of the park that Alycee likes to visit?

<span class="label">Method</span>

This challenge was part of the wider _Contact Tracing_ series, and Part I asked
us to find Alycee's art account.

Having previously done this, _"**JakePaul**"_ had the art account link and there
are a few examples of drawings that she enjoys making. One of them was
_["My favourite place"](https://www.deviantart.com/alyceedoesstem/art/My-Favorite-Place-832559463)_.

Upon closer inspection, this image contained a set of coordinates:

```
 38° 01' 27.8" S  (latitude)
145° 20' 29.0" E  (longitude)
```

I'm a very decimal kind of person, much the same as most technology that deals
with maps so I opened up a
[GPS coordinates converter](https://www.gps-coordinates.net/gps-coordinates-converter)
which provided the correct decimal conversions from the original degrees,
minutes and seconds. Google maps does also accept the old-style
coordinates but without the degree symbol readily available on my keyboard, I
just went for the quick and easy solution of getting it converted. :man-shrugging:

{{<search "!maps 38°01'27.8\"S 145°20'29.0\"E">}}

Those coordinates led us to
[Wilson Botanic Park](https://www.google.com.au/maps/place/38°01'27.8"S+145°20'29.0"E/)
on Google Maps, so boom, another flag.

{{% /ctf/container %}}

{{<spacer "left">}}



## Contact Tracing - Part II

{{%ctf/container points="100" category="Social" flag="CYBAR{Kilauea}" %}}

<span class="label">Description</span>

>We need more locations Alycee may have or will visit in the future.<br/>
>What is the exact name of the volcano that Alycee visited?

<span class="label">method</span>

For this challenge, once again we are hanging out on Alycee's
[deviantart page](https://www.deviantart.com/alyceedoesstem) (she's pretty great
at drawing, I won't lie).

[The day it all changed](https://www.deviantart.com/alyceedoesstem/art/The-day-it-all-changed-832327285)
is a drawing of an erupting volcano in Hawaii, and much like Part III, there is
information hidden within the drawing: A date, `30 April 2018`.

Using the query: {{<search "hawaii volcano eruption \"30 april 2018\"">}}
on **duckduckgo**, we scroll down a few times and find a wikipedia article about
the [2018 lower Puna eruption](https://en.wikipedia.org/wiki/2018_lower_Puna_eruption)
which mentions **Kilauea** by name at the correct point in time,
so we have the name of our volcano :volcano:.

{{% /ctf/container %}}

{{<spacer "left">}}



## WFH (EoM) - Part 1

{{%ctf/container points="350" category="Social" flag="CYBAR{QV1}" %}}

<span class="label">Description</span>

> Contact Tracing continues. We need to locate Marc's home and evacuate the neighborhood/building and place them in isolation.<br/>
> What's the name of the building Marc lives in?

<span class="label">Method</span>

Oof, okay, this one was pretty tough, not going to lie. We're still dealing
with the twitter peep [@HevisMarc](https://twitter.com/HevisMarc), so that at
least makes things a bit easier, no new people. Our biggest data source on Marc
has been twitter so we'll keep rolling with that until it runs out.

If we search twitter with: {{<search "!tw from:HevisMarc">}} we can go through all the posts that this account has ever made, and one of
them has [a video taken from a balcony](https://twitter.com/HevisMarc/status/1234802973189890048).

<div class="mv4 db mw-100">{{< tweet 1234802973189890048 >}}</div>
<div class="cb"></div>

It's possible that an office building _could_ have a balcony, but it seems much
more reasonable to assume that this is in fact taken from a place of residence.

Okay, so we have found the video and we've got a pretty strong case that this
will be the correct source to show where Marc lives, now we have to geolocate
the building from which the video was taken, to figure out where his house is.

One of the most distinctive features in the footage is a building that has a
face as part of the facade.

If we drop: {{<search "\"building with a face\"">}} into **duckduckgo**, we discover that this building is located in Melbourne,
Australia, [off Swanston St](https://www.google.com.au/maps/place/The+Barak+Building/@-37.8057812,144.9604622,17z/data=!3m1!4b1!4m5!3m4!1s0x6ad64376f93470cd:0xf688b7bc56fe0699!8m2!3d-37.8057812!4d144.9626509?hl=en) in the CBD. This fits the profile of the twitter
account, so things are starting to piece together now.

We have a rough idea of where this took place, we now need to find the correct
perspective in order to establish which building in particular it came from.

I went into 3d satellite google maps to check this out:

![Getting the initial angle](images/wfh01-1.png "So there's now a rough approximation on the angle, I then zoomed out until I found some kind of building to pin the angle to.")

![Looking over the shoulder](images/wfh01-2.png "This angle is sort of _\"looking over the shoulder\"_ of a building, which confirms that the angle is correct and there is a viable building to pin it to.")

![Top-down view](images/wfh01-3.png "If we look at that building from a top down view, it comes highlighted as QV1 Melbourne.")

Turns out there are actually more than one QV1's in Australia, so that caught
me out once :grimacing:

Anyway, we have the building's name now: **QV1**.<br/>
Nice, a tricky but very fun challenge.

{{% /ctf/container %}}

{{<spacer "left">}}



## WFH (EoM) - Part 2

{{%ctf/container points="100" category="Social" flag="CYBAR{44}" %}}

<span class="label">Description</span>

> We need more information on the building to work out the level it's being potentially filmed from. How many levels (above ground) does the building have?

<span class="label">Method</span>

Well, the good news is we've already done most of the hard work for this
challenge in [part 1](#wfh-eom---part-1).

On **duckduckgo** we put in: {{<search "qv1 melbourne storeys">}}

We end up with the third result being a link to the architects who [designed the
building](https://www.johnwardlearchitects.com/projects/qv1-residential-tower/)
that we're looking into.

It turns out we actually end up with essentially two flags in one on this page
as what we need for the [next challenge](#wfh-eom---part-3) is available here too, but for this one
at least, we know the number of storeys now: **44**.

{{% /ctf/container %}}

{{<spacer "left">}}



## WFH (EoM) - Part 3

{{%ctf/container points="50" category="Social" flag="CYBAR{2005}" %}}

<span class="label">Description</span>

> Alright, we need to figure out how long Marc has lived there for, and the earliest he could have moved in. What was the year the building was finally built in?

<span class="label">Method</span>

Not much method here, we refer to the resources from the
[previous challenge](./#wfh-eom---part-2), and we have a completion date of
**2005** for the build of the project, so that would make it the earliest possible
year that Marc could have moved in there.

{{% /ctf/container %}}

{{<spacer "left">}}



## Fake News

{{%ctf/container points="650" category="General" flag="CYBAR{14}" %}}

<span class="label">Description</span>

>We've just received a report of The Daily News publishing an article that is causing a lot of concern and fear in the public. Given its wording and theme, we are sure it's fake news generated by the Roomba. However, TDN will not disclose their source. Here's the article, we need you to find the exact number of people that went through Southern Cross Station at the exact time referenced so we can determine if the article is fake. SX Station has released a statement saying that all footage of that night has been deleted so we can't rely on visuals.
>
>Article text:
> >_"Wild scenes as 40 people confirmed to be infected with COVID-19 ran through Southern Cross Station at 4:00am on Friday, the 28th of February 2020. The frightening witness account has caused panic buying at stores around the country as people prepare to stay indoors. Our source confirms they were the only witness and that this infectious routine could be happening at other major transport venues through the country without the public's knowledge."_
>
>Find the exact number of pedestrians that walked through Southern Cross Station that morning at 4am, on Friday, the 28th of February.

<span class="label">Method</span>

What the <span class="tracked-mega">heck</span>!?

Definitely a good reason why this was set to 650 points, from the first time
hearing it, this challenge sounds basically impossible. Let's break it down.

The good news is, we have something very specific to shoot for, we know the
exact target so we don't have to use too much guesswork to know if we've
arrived at the right solution, the problem in this challenge is mostly in the
_"where do we find this information?"_.

I took a number of failed approaches here, I copied different excerpts of the
article text and tried to get something out of that with no luck. Searching for
{{<search "COVID-19 southern cross news">}} also failed pretty
spectacularly.

I took some time away and worked on other tasks before coming back to this one,
and started to reason about what was the information I was really after.

I entered:

{{<search "southern cross foot traffic">}}

...as a query into **duckduckgo**. After scrolling down a tiny bit, I stumbled on
[a news article](https://www.theage.com.au/national/victoria/pedestrian-numbers-drop-in-the-cbd-as-pandemic-keeps-people-home-20200317-p54azk.html)
from the Herald Sun (a local Melbourne newspaper). Reading through the article,
they had graphs showing levels of foot traffic through various places
throughout the city, one of them including Southern Cross station. The source
that was quoted for these graphs was the _City of Melbourne_. Time to go
data hunting.

Another search:

{{<search "city of melbourne data">}}

Oh? They have [a whole website](https://data.melbourne.vic.gov.au) dedicated to city-related data, and it's
completely open access? Excellent, just what we need!

An internal site search for <span class="b code">foot traffic</span> returned a
[dataset that contained hourly pedestrian
counts](https://data.melbourne.vic.gov.au/Transport/Pedestrian-Counting-System-2009-to-Present-counts-/b2ak-trbp) since 2009.

After using the site to filter out the data to get to the point in time in
question, we arrived on our magic number: **14**. Not very many people to be
going through the station at that time of the day, but it is 4AM, so that sort
of makes sense.

Turns out that The Daily News' article was definitely fake, we found the
official data to prove it ...unless _that_ was fake too? :scream:

{{% /ctf/container %}}

{{<spacer "left">}}



## Pretty Fly for a WiFi

{{%ctf/container points="250" category="Social" flag="CYBAR{Ballarat}" %}}

<span class="label">Description</span>

> We need to find Marc's second office location (not the primary workplace) for the contract tracing. Business records tell us it's relatively new. Scour his Twitter account and see if there's anything that can help us geo-locate it. We don't need it down to the road, just the town (not suburb) and we can work from there.

<span class="label">Method</span>

Ahh Marc, I will miss you after this.

We need to find Marc's _second_ office location, and judging by the challenge
name, we're going to be looking for something to do with wifi.

We scroll through his tweets one last time and we stumble on this:

<div class="mv4 db mw-100">{{< tweet 1234814343851589633 >}}</div>
<div class="cb"></div>

I hope that Marc does finally end up with 100Mbps speeds, he seems like a
pretty good guy, and we all deserve to have the internet speeds that we dream
of. _But that doesn't stop me from wanting to pinpoint the location of this network
that he's on._

He's given us the BSSID and the SSID, both of which are pretty handy in
narrowing down his location using a tool like [wigle.net](https://wigle.net/).

We do need to make an account for the service in order to use the advanced
search features, but that's fine. Once we do, we can use the advanced search to fill in his BSSID and SSID,
and hey presto, we end up with his network pinpointed on
[the map](https://wigle.net/map?maplat=-37.57155608999937&maplon=143.85816955999755&mapzoom=20&coloring=density).

If we zoom out a bit, we can see that the town in question is **Ballarat**.

Game, set, match<span class="i o-20">(ed by BSSID)<span>

{{% /ctf/container %}}

{{<spacer "left">}}



# Summary

This event was freaking _awesome_. The first time I've ever done an OSINT
CTF and I loved it. There were some really weird and funky challenges, but it
was almost like a big treasure hunt on the internet.

In the end our team managed to complete all but _one_ of the challenges which
was really cool. The one we couldn't manage was
[Curious Case of COVID](https://github.com/cybar-party/cybar-osint-ctf-2020/blob/master/Challenges/General/Curious%20Case%20of%20COVID.md),
but honestly I'm not even mad about that because we gave it a massive go
and for our first time doing something like this, I'm really proud of where we
placed in the end.

Super excited for the next OSINT challenge, the best part of these is that you
don't need to be technically-minded to be able to do them, you just have
to have a reasonable methodology of how to find out certain bits of
information and how to deduct a result from the available resources.

My team and I came in [**21st** position](https://github.com/cybar-party/cybar-osint-ctf-2020/blob/master/Results.md) with a whopping **4225** points, and
I'm really proud of that achievement because we worked super well together.
Thanks again to the people mentioned at the top of this post, I had a great
time and your hard work and dedication is seriously appreciated.

{{<spacer "left">}}

# Some extra tidbits

I figured I would drop a few tools I used which made this whole event a lot
easier for me.

## Tools

### Duckduckgo

Not only is this a pretty great search engine on its own, but it also gives you
internet-searching super powers through the use of their ["bang"
functionality](https://duckduckgo.com/bang).

You can read more about bang [here](https://duckduckgo.com/bang), but basically
it gives you shortcut access to a vast array of search functions on various
websites. So if I wanted to get Google Maps of Mexico City, I could do:

{{<search "!maps Mexico City">}}

...and it will deliver you to the page (via duckduckgo).

(I have duckduckgo as my
default search engine in Firefox so I can type this straight into the url bar
and away I go, super quick and handy.

### TinEye

This is a [reverse image search](https://tineye.com), so you can upload an image and find where else
it has been use on the web. I didn't end up using it for the challenges that I
managed to solve, but it was good when helping out teammates and thinking out
of the box a few times.
